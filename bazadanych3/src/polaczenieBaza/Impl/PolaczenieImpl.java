package polaczenieBaza.Impl;

//import java.sql.Connection;
import java.sql.Date;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;

import polaczenieBaza.spec.PolaczenieSpec;

//import com.mysql.jdbc.Driver;

public class PolaczenieImpl implements PolaczenieSpec {

	java.sql.Connection connection = null;
	Statement stmt = null;
	ResultSet rs = null;

	public PolaczenieImpl() {

	}

	public Object wykonaj(String statement, String zwracany) {
		int wynik = 0;
		String table = null;
		try {
			Class.forName("com.mysql.jdbc.Driver").newInstance();

			String connectionUrl = "jdbc:mysql://37.187.99.150:3306/plakaty";
			String connectionUser = "plakaty";
			String connectionPassword = "plakatyplakaty110";
			connection = DriverManager.getConnection(connectionUrl,
					connectionUser, connectionPassword);
			stmt = connection.createStatement();

			if (zwracany == "wykonaj-zapisz-int") {
				wynik = stmt.executeUpdate(statement);
				return wynik;
			} else if (zwracany == "wykonaj-odczyt-int") {
				ResultSet resultset = stmt.executeQuery(statement);
				if (resultset.next())
					return resultset.getString(1);
				return 1;
			} else if (zwracany == "wykonaj-jtable") {
				rs = stmt.executeQuery(statement);
				table = zbudujTabele(rs, false);
				return table;
			} else if (zwracany == "wykonaj-jtable-zad") {
				rs = stmt.executeQuery(statement);
				table = zbudujTabele(rs, true);
				return table;
			} else if (zwracany == "wykonaj-jtable-przycisk") {
				rs = stmt.executeQuery(statement);
				table = TabelaPrzycisk(rs);
				return table;
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {// zamykani polaczenia
			try {
				if (rs != null)
					rs.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
			try {
				if (stmt != null)
					stmt.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
			try {
				if (connection != null)
					connection.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return "B��d";
	}

	long Fdays = 4 * 24 * 60 * 60 * 1000;
	long week = 7 * 24 * 60 * 60 * 1000;

	private String zbudujTabele(ResultSet rs, boolean zad) throws SQLException {

		ResultSetMetaData metaData = rs.getMetaData();

		String wynik = "<table border=\"3\" style=\"background-color:#FFFFCC;border-collapse:collapse;border:3px solid #FFCC00;color:#000000;width:70%\" cellpadding=\"3\" cellspacing=\"3\">"
				+ "<tr>";
		// names of columns

		// Vector<String> columnNames = new Vector<String>();
		int columnCount = metaData.getColumnCount();
		for (int column = 3; column <= columnCount; column++) {

			wynik += "<td><b>" + metaData.getColumnName(column) + "</b></td>";
		}
		wynik += "</tr>";

		// data of the table
		// Vector<Vector<Object>> data = new Vector<Vector<Object>>();
		while (rs.next()) {

			if (zad) {
				Timestamp data = (Timestamp) rs.getObject(2);
				java.util.Date today = new java.util.Date();
				Timestamp CurData = new java.sql.Timestamp(today.getTime());

				long time = CurData.getTime() - data.getTime();

				if (((String) rs.getObject(8)).equals("Zako�czone")) {
					wynik += "<tr bgcolor=\"#00FF00\">";
				} else if (time > week) {
					wynik += "<tr bgcolor=\"#FF0000\">";
				} else if (time > Fdays) {
					wynik += "<tr bgcolor=\"#FF7F00\">";
				} else {
					wynik += "<tr bgcolor=\"#00FF00\">";
				}
			}else{
				wynik += "<tr>";
			}
			// Vector<Object> vector = new Vector<Object>();
			for (int columnIndex = 3; columnIndex <= columnCount; columnIndex++) {
				wynik += "<td>" + rs.getObject(columnIndex) + "</td>";
				// vector.add(rs.getObject(columnIndex));
			}
			wynik += "</tr>";
			// data.add(vector);

		}
		wynik += "</table>";

		return wynik;

		// return new DefaultTableModel(data, columnNames);

	}

	private String TabelaPrzycisk(ResultSet rs) throws SQLException {

		ResultSetMetaData metaData = rs.getMetaData();
		String stat = "";
		if (rs.next()) {
			stat = (String) rs.getObject(5);
			rs.previous();
		}
		boolean StatZad = true;
		String dzial = "";
		if (stat.equals("Wolne")) {
			StatZad = true;
			dzial = "Podejmij";
		} else if (stat.equals("Podj�te")) {
			StatZad = false;
			dzial = "Zako�cz";
		}

		System.out.println(stat);

		String wynik = "<form method=\"POST\" action=\"";
		if (StatZad)
			wynik += "UserWolneZadania\" ";
		else
			wynik += "OpiszZadanie\" ";
		wynik += "id=\"form1\"><table border=\"3\" style=\"background-color:#FFFFCC;border-collapse:collapse;border:3px solid #FFCC00;color:#000000;width:70%\" cellpadding=\"3\" cellspacing=\"3\">"
				+ "<tr>";
		// names of columns
		int ID = 0;

		// Vector<String> columnNames = new Vector<String>();
		int columnCount = metaData.getColumnCount();
		for (int column = 3; column <= columnCount; column++) {

			wynik += "<td><b>" + metaData.getColumnName(column) + "</b></td>";
		}
		wynik += "<td><b>Edycja</b></td>";
		wynik += "</tr>";

		// data of the table
		// Vector<Vector<Object>> data = new Vector<Vector<Object>>();

		while (rs.next()) {

			Timestamp data = (Timestamp) rs.getObject(2);
			java.util.Date today = new java.util.Date();
			Timestamp CurData = new java.sql.Timestamp(today.getTime());

			long time = CurData.getTime() - data.getTime();

			if (time > week) {
				wynik += "<tr bgcolor=\"#FF0000\">";
			} else if (time > Fdays) {
				wynik += "<tr bgcolor=\"#FF7F00\">";
			} else {
				wynik += "<tr bgcolor=\"#00FF00\">";
			}

			// Vector<Object> vector = new Vector<Object>();
			ID = (int) rs.getObject(1);

			for (int columnIndex = 3; columnIndex <= columnCount; columnIndex++) {
				wynik += "<td>" + rs.getObject(columnIndex) + "</td>";
				// vector.add(rs.getObject(columnIndex));
			}
			wynik += "<td>"
					+ "<button type=\"submit\" name=\"ID\"\" form=\"form1\" value=\""
					+ ID + "\">" + dzial + "</button></td>";
			wynik += "</tr>";
			// data.add(vector);

		}
		wynik += "</form></table>";

		return wynik;

		// return new DefaultTableModel(data, columnNames);

	}

}