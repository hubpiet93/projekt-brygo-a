package zarzadzanie.Impl;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import html.*;

public class SimpleServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {

		AdministratorImpl admin = new AdministratorImpl();

		Szablony szablon = new Szablony();

		String doWyswietlenia = szablon.zamiana(admin.getUzytkownicy());

		HttpSession session = req.getSession();

		resp.setContentType("text/html");
		resp.setCharacterEncoding("UTF-8");
		resp.getWriter().write(doWyswietlenia);
	}
}