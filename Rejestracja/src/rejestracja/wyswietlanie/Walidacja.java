package rejestracja.wyswietlanie;

import java.util.HashMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Walidacja {

	private HashMap<String, String> dictionary = new HashMap<String, String>() {

		private static final long serialVersionUID = 1L;
	};

	public Walidacja() {
		this.dictionary
				.put("Login", "^[A-Za-z0-9+_.-����󜟿�ʣ�ӌ��]{6,45}$");
		this.dictionary.put("Has�o", "^[A-Za-z0-9+_.-]{6,45}$");
		this.dictionary.put("Imi�", "^[A-Za-z����󜟿�ʣ�ӌ��]{2,60}$");
		this.dictionary.put("Nazwisko", "^[A-Za-z����󜟿�ʣ�ӌ��]{2,60}$");
		this.dictionary.put("Telefon", "^[0-9]{7,9}$");
		this.dictionary
				.put("Mail",
						"^[\\w!#$%&'*+/=?`{|}~^-]+(?:\\.[\\w!#$%&'*+/=?`{|}~^-]+)*@(?:[a-zA-Z0-9-]+\\.)+[a-zA-Z]{2,6}$");
		this.dictionary
				.put("Miasto",
						"^[a-zA-Z����󜟿�ʣ�ӌ��]+(?:[\\s-][a-zA-Z����󜟿�ʣ�ӌ��]+)*$");
		this.dictionary.put("Data urodzenia",
				"^([0-9]{4}/[0-1][0-9]/[0-9]{2})$");
	}

	public boolean sprawdzPole(String pole, String wartosc) {

		String regex = this.dictionary.get(pole);
		Pattern pattern = Pattern.compile(regex);
		Matcher matcher = pattern.matcher(wartosc);
		return matcher.matches();
	}
}
