package zakladki.wyswietlanie;

import html.Szablony;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import polaczenieBaza.Impl.PolaczenieImpl;
import zarzadzanie.Impl.Userzy;

public class UserMojeZadaniaAktywne extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {

		HttpSession session = req.getSession(true);

		String login = (String) session.getAttribute("login");
		String haslo = (String) session.getAttribute("haslo");

		PolaczenieImpl polaczenie = new PolaczenieImpl();
		Userzy user = new Userzy(login, haslo);
		Szablony szablon = new Szablony();

		if (user.sprawdz_login_haslo() == 1) {

			System.out.println("POBIERAM ZADANIA");

			String zawartosc = szablon
					.zamiana("<b>Na tej stronie znajduj� si� Twoje zadania, kt�rych realizacji si� podj��e�. Mo�esz w tym miejscu ustawi� zako�czenie ich wykonywania i opisa� przebieg zadania.</b></br>"
							+ (String) polaczenie
									.wykonaj(
											"SELECT Zadania.id_zadania, Statusy_Zadan.data_statusu, Zadania.opis_zadania, Statusy_Zadan.data_statusu, Statusy.nazwa_statusu  "
													+ "FROM Statusy_Zadan, Statusy, Zadania, Userzy                                             "
													+ "WHERE Userzy.login =  '"
													+ login
													+ "'                                                            "
													+ "AND Userzy.id_uzytkownika = Zadania.Userzy_id_uzytkownika                                "
													+ "AND Statusy.id_status =  '2'                                                   "
													+ "AND Zadania.id_zadania = Statusy_Zadan.Zadania_id_zadania                                "
													+ "AND Statusy_Zadan.Statusy_id_status = Statusy.id_status",
											"wykonaj-jtable-przycisk")
											+"</br></br><table><tr><td width=\"50px\" bgcolor=\"#00FF00\"></td><td>Nowe zadanie, masz sporo czasu</td>"
											+"<tr><td width=\"50px\" bgcolor=\"#FF7F00\"></td><td>Niebawem minie deadline, po�piesz si�!</td>"
											+"<tr><td width=\"50px\" bgcolor=\"#FF0000\"></td><td>Przekroczy�e� czas wykonywania zadania, zr�b je natychmiast!</td></table>");

			resp.setContentType("text/html");
			resp.setCharacterEncoding("UTF-8");
			resp.getWriter().write(zawartosc);
		} else {
			resp.sendRedirect("/zaloguj");
		}

	}
}