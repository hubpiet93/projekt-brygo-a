package zakladki.wyswietlanie;

import html.Szablony;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import polaczenieBaza.Impl.PolaczenieImpl;
import polaczenieBaza.spec.PolaczenieSpec;
import zarzadzanie.Impl.Userzy;

public class DodajZlecenie extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		String miasto = req.getParameter("miasto");
		String telefon = req.getParameter("telefon_zleceniodawcy");
		String mail = req.getParameter("mail_zleceniodawcy");
		String opis = req.getParameter("opis_zadania");

		PolaczenieSpec polaczenie = new PolaczenieImpl();

		polaczenie
				.wykonaj(
						"INSERT INTO `plakaty`.`Zadania` (`id_zadania` ,`Userzy_id_uzytkownika` ,`miasto` ,`telefon_zleceniodawcy` ,`mail_zleceniodawcy` ,`opis_zadania` ,`adres_plik` ,`opis`)"
								+ "VALUES (NULL , NULL , '"
								+ miasto
								+ "', '"
								+ telefon
								+ "', '"
								+ mail
								+ "', '"
								+ opis
								+ "', NULL, NULL)", "wykonaj-zapisz-int");

		String IDZadania = (String) polaczenie
				.wykonaj(
						"SELECT id_zadania FROM `plakaty`.`Zadania` WHERE `Userzy_id_uzytkownika` is NULL AND `miasto` = '"
								+ miasto
								+ "' AND `telefon_zleceniodawcy`= '"
								+ telefon
								+ "' AND `mail_zleceniodawcy`= '"
								+ mail
								+ "' AND `opis_zadania` = '"
								+ opis
								+ "' AND `adres_plik` is NULL AND `opis` is NULL",
						"wykonaj-odczyt-int");

		polaczenie
				.wykonaj(
						"INSERT INTO `plakaty`.`Statusy_Zadan` (`id_statusy` ,`Statusy_id_status` ,`Zadania_id_zadania` ,`data_statusu`)"
								+ "VALUES (NULL , '1' , '"
								+ IDZadania
								+ "', NOW( ))", "wykonaj-zapisz-int");
		
		resp.setContentType("text/html");
		resp.setCharacterEncoding("UTF-8");
		resp.getWriter()
				.write("<HTML><HEAD>"
						+ "<script type=\"text/javascript\">"
						+ "alert(\"Dodano zadanie!\");"
						+ "window.location=\"/WszystkieZadania\"" + "</script>"
						+ "</HEAD></HTML>");


		System.out.println("Dodano Zadanie");
	}

	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {

		HttpSession session = req.getSession(true);

		String login = (String) session.getAttribute("login");
		String haslo = (String) session.getAttribute("haslo");

		Userzy user = new Userzy(login, haslo);
		Szablony szablon = new Szablony();

		if (user.sprawdz_login_haslo() == 1 && login.equals("admin")) {
			String zawartosc = szablon
					.zamiana("<div id=\"form_container\">                                                                                                             "
							+ "	                                                                                                                                    "
							+ "		<h1><a>Dodaj nowe zlecenie</a></h1>                                                                                                   "
							+ "		<form id=\"form_953125\" class=\"appnitro\"  method=\"post\" action=\"\">                                                       "
							+ "										                                                                                                    "
							+ "			<ul >                                                                                                                       "
							+ "			                                                                                                                            "
							+ "					<li id=\"li_1\" >                                                                                                   "
							+ "		<label class=\"description\" for=\"miasto\">Miasto </label>                                                                  "
							+ "		<div>                                                                                                                           "
							+ "			<input id=\"miasto\" name=\"miasto\" class=\"element text medium\" type=\"text\" maxlength=\"255\" value=\"\"/>       "
							+ "		</div>                                                                                                                          "
							+ "		</li>		<li id=\"li_2\" >                                                                                                   "
							+ "		<label class=\"description\" for=\"telefon_zleceniodawcy\">Telefon Zleceniodwacy </label>                                                   "
							+ "		<div>                                                                                                                           "
							+ "			<input id=\"telefon_zleceniodawcy\" name=\"telefon_zleceniodawcy\" class=\"element text medium\" type=\"text\" maxlength=\"255\" value=\"\"/>       "
							+ "		</div>                                                                                                                          "
							+ "		</li>		<li id=\"li_3\" >                                                                                                   "
							+ "		<label class=\"description\" for=\"mail_zleceniodawcy\">Mail Zleceniodawcy                                                               "
							+ " </label>                                                                                                                               "
							+ "		<div>                                                                                                                           "
							+ "			<input id=\"mail_zleceniodawcy\" name=\"mail_zleceniodawcy\" class=\"element text medium\" type=\"text\" maxlength=\"255\" value=\"\"/>       "
							+ "		</div>                                                                                                                          "
							+ "		</li>		<li id=\"li_4\" >                                                                                                   "
							+ "		<label class=\"description\" for=\"opis_zadania\">Opis zadania </label>                                                            "
							+ "		<div>                                                                                                                           "
							+ "			<input id=\"opis_zadania\" name=\"opis_zadania\" class=\"element text medium\" type=\"text\" maxlength=\"255\" value=\"\"/>       "
							+ "		</div>                                                                                                                          "
							+ "		</li>                                                                                                                           "
							+ "			                                                                                                                            "
							+ "					<li class=\"buttons\">                                                                                              "
							+ "			                                                               "
							+ "			                                                                                                                            "
							+ "				<input id=\"saveForm\" class=\"button_text\" type=\"submit\" name=\"submit\" value=\"Submit\" />                        "
							+ "		</li>                                                                                                                           "
							+ "			</ul>                                                                                                                       "
							+ "		</form>	                                                                                                                        "
							+ "		<div id=\"footer\">                                                                                                             "
							+ "			                                                                 "
							+ "		</div>                                                                                                                          "
							+ "	</div>                                                                                                                              ");

			resp.setContentType("text/html");
			resp.setCharacterEncoding("UTF-8");
			resp.getWriter().write(zawartosc);
		} else if (user.sprawdz_login_haslo() == 1) {
			resp.setContentType("text/html");
			resp.setCharacterEncoding("UTF-8");
			resp.getWriter()
					.write("<HTML><HEAD>"
							+ "<script type=\"text/javascript\">"
							+ "alert(\"Nie masz uprawnie� do odwiedzania tej strony!\");"
							+ "window.location=\"/\"" + "</script>"
							+ "</HEAD></HTML>");
		} else {
			resp.sendRedirect("/zaloguj");
		}

	}
}