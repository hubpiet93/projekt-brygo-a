package zakladki.wyswietlanie;

import html.Szablony;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import polaczenieBaza.Impl.PolaczenieImpl;
import zarzadzanie.Impl.Userzy;

public class UserWolneZadania extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {

		HttpSession session = req.getSession(true);

		String login = (String) session.getAttribute("login");
		String haslo = (String) session.getAttribute("haslo");

		PolaczenieImpl polaczenie = new PolaczenieImpl();
		Userzy user = new Userzy(login, haslo);
		Szablony szablon = new Szablony();

		if (user.sprawdz_login_haslo() == 1) {

			String zawartosc = szablon
					.zamiana("<b>Widzisz tutaj wolne zadania, kt�rych realizacji mo�esz si� podj��, klikaj�c w odpowiedni przycisk</br></b>"
							+ (String) polaczenie
									.wykonaj(
											"SELECT Zadania.id_zadania, Statusy_Zadan.data_statusu, Zadania.miasto, Zadania.opis_zadania, Statusy.nazwa_statusu      "
													+ "FROM Statusy_Zadan, Statusy, Zadania, Userzy                                                                            "
													+ "WHERE Userzy.login =  '"
													+ login
													+ "'                                                                                           "
													+ "AND Statusy.id_status =  '1'                                                                                            "
													+ "AND Zadania.id_zadania = Statusy_Zadan.Zadania_id_zadania                                                               "
													+ "AND Statusy_Zadan.Statusy_id_status = Statusy.id_status                                                                 "
													+ "AND Zadania.miasto = Userzy.miasto                                                                                      ",
											"wykonaj-jtable-przycisk")
							+ "</br></br><table><tr><td width=\"50px\" bgcolor=\"#00FF00\"></td><td>Nowe zadanie, d�ugi termin aktywno�ci</td>"
							+ "<tr><td width=\"50px\" bgcolor=\"#FF7F00\"></td><td>Niebawem zadanie zostanie przedawnione</td>"
							+ "<tr><td width=\"50px\" bgcolor=\"#FF0000\"></td><td>Zadanie d�ugi czas oczekuje na przyj�cie, lada moment zostanie przedawnione</td></table>");

			resp.setContentType("text/html");
			resp.setCharacterEncoding("UTF-8");
			resp.getWriter().write(zawartosc);

		} else {
			resp.sendRedirect("/zaloguj");
		}

	}

	protected void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {

		HttpSession session = req.getSession(true);

		String login = (String) session.getAttribute("login");
		String haslo = (String) session.getAttribute("haslo");
		String idUzytkownika = (String) session.getAttribute("id");

		PolaczenieImpl polaczenie = new PolaczenieImpl();
		Userzy user = new Userzy(login, haslo);

		if (user.sprawdz_login_haslo() == 1) {

			String IdZadania = (String) req.getParameter("ID");

			if (IdZadania != null) {

				String ZadUsr = "cos";
				ZadUsr = (String) polaczenie.wykonaj(
						"SELECT Userzy_id_uzytkownika FROM Zadania WHERE Zadania.id_zadania='"
								+ IdZadania + "'", "wykonaj-odczyt-int");

				System.out.println(ZadUsr);
				if (ZadUsr == null) {
					polaczenie.wykonaj(
							"UPDATE Zadania SET Userzy_id_uzytkownika='"
									+ idUzytkownika + "' WHERE id_zadania='"
									+ IdZadania + "'", "wykonaj-zapisz-int");
					
					polaczenie
							.wykonaj(
									"UPDATE Statusy_Zadan SET Statusy_id_status='2', data_statusu=NOW() WHERE Zadania_id_zadania='"
											+ IdZadania + "'",
									"wykonaj-zapisz-int");
					
					resp.setContentType("text/html");
					resp.setCharacterEncoding("UTF-8");
					resp.getWriter()
							.write("<HTML><HEAD>"
									+ "<script type=\"text/javascript\">"
									+ "alert(\"Przyj��e� zadanie do wykonania.\");"
									+ "window.location=\"/UserWolneZadania\""
									+ "</script>" + "</HEAD></HTML>");
					
					System.out.println("Zarejestrowano u�ytkownika "
							+ user.login);
					System.out.println("Zadanie z ID ==" + IdZadania
							+ "zosta�a przydzielona u�ytkownikowi");
				}else{
					resp.setContentType("text/html");
					resp.setCharacterEncoding("UTF-8");
					resp.getWriter()
							.write("<HTML><HEAD>"
									+ "<script type=\"text/javascript\">"
									+ "alert(\"Za p�no, kto� ju� zaj�� to zadanie!\");"
									+ "window.location=\"/UserWolneZadania\""
									+ "</script>" + "</HEAD></HTML>");
				}
			} else {
				resp.sendRedirect("/UserWolneZadania");
			}
		} else {
			resp.sendRedirect("/zaloguj");
		}

	}
}