package zakladki.wyswietlanie;

import java.io.IOException;

import javax.servlet.ServletException;

import org.osgi.service.http.HttpService;
import org.osgi.service.http.NamespaceException;

public class Wyswietl {

	private HttpService httpService;

	public void setHttpService(HttpService httpService) {
		this.httpService = httpService;
	}

	protected void startup() throws IOException {
		try {
			WszyscyUzytkownicy wszyscyUzytkownicy = new WszyscyUzytkownicy();
			WszystkieZadania wszystkieZadania = new WszystkieZadania();
			UserWolneZadania userWolneZadania = new UserWolneZadania();
			UserMojeZadaniaAktywne userMojeZadania = new UserMojeZadaniaAktywne();
			UserZakonczoneZadania userZakonczoneZadania = new UserZakonczoneZadania();
			UserKonto userKonto = new UserKonto();
			DodajZlecenie dodajZlecenie = new DodajZlecenie();
			OpiszZadanie opiszZadanie = new OpiszZadanie();
			
			httpService.registerServlet("/WszyscyUzytkownicy", wszyscyUzytkownicy, null, null);
			httpService.registerServlet("/WszystkieZadania", wszystkieZadania, null, null);
			httpService.registerServlet("/UserWolneZadania", userWolneZadania, null, null);
			httpService.registerServlet("/UserMojeZadania", userMojeZadania, null, null);
			httpService.registerServlet("/UserZakonczoneZadania", userZakonczoneZadania, null, null);
			httpService.registerServlet("/UserKonto", userKonto, null, null);
			httpService.registerServlet("/DodajZlecenie", dodajZlecenie, null, null);
			httpService.registerServlet("/OpiszZadanie", opiszZadanie, null, null);
		} catch (ServletException e) {
			e.printStackTrace();
		} catch (NamespaceException e) {
			e.printStackTrace();
		}
	}

	protected void shutdown() {
		httpService.unregister("/WszyscyUzytkownicy");
		httpService.unregister("/WszystkieZadania");
		httpService.unregister("/UserWolneZadania");
		httpService.unregister("/UserMojeZadania");
		httpService.unregister("/UserZakonczoneZadania");
		httpService.unregister("/UserKonto");
		httpService.unregister("/DodajZlecenie");
		httpService.unregister("/OpiszZadanie");
	}

}