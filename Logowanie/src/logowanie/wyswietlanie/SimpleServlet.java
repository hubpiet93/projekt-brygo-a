package logowanie.wyswietlanie;

import java.io.IOException;

import html.*;
import zarzadzanie.Impl.*;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import polaczenieBaza.Impl.PolaczenieImpl;

public class SimpleServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		String login = req.getParameter("login");
		String haslo = req.getParameter("haslo");

		Userzy user = new Userzy(login, haslo);

		PolaczenieImpl polaczenie = new PolaczenieImpl();
		resp.setContentType("text/html");
		if (user.sprawdz_login_haslo() == 1) {

			HttpSession session = req.getSession(true);
			String id = (String) polaczenie.wykonaj(
					"SELECT Userzy.id_uzytkownika FROM Userzy WHERE login='"
							+ login + "' AND haslo='" + haslo + "'",
					"wykonaj-odczyt-int");
			session.setAttribute("login", req.getParameter("login"));
			session.setAttribute("haslo", req.getParameter("haslo"));
			session.setAttribute("id", id);

		}
		System.out.println(user.sprawdz_login_haslo() + ""); // 1 - poprawne
																// dane, 0
																// -niepoprawne

		if (user.sprawdz_login_haslo() == 1) {
			resp.sendRedirect("/");
		}else{
			resp.setContentType("text/html");
			resp.setCharacterEncoding("UTF-8");
			resp.getWriter().write(
					"<HTML><HEAD>" + "<script type=\"text/javascript\">"
							+ "alert(\"Niepoprawne login lub has�o!\");"
							+ "window.location=\"/zaloguj\""
							+ "</script>" + "</HEAD></HTML>");
		}

		resp.getWriter().write(user.sprawdz_login_haslo());
	}

	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {

		String wyloguj = req.getParameter("wyloguj");

		if (wyloguj != null) {
			HttpSession session = req.getSession();
			session.invalidate();

			resp.sendRedirect("/");
			return;
		}

		HttpSession session = req.getSession(true);

		String login = (String) session.getAttribute("login");
		String haslo = (String) session.getAttribute("haslo");

		Userzy user = new Userzy(login, haslo);

		if (user.sprawdz_login_haslo() == 1) {

			resp.getWriter().write(
					"<HTML><HEAD>" + "<script type=\"text/javascript\">"
							+ "alert(\"Wcze�niej si� wyloguj!\");"
							+ "window.location=\"/UserMojeZadania\""
							+ "</script>" + "</HEAD></HTML>");

		}

		resp.setContentType("text/html");

		Szablony szablon = new Szablony();

		String zawartosc = szablon
				.zamiana("	<div id=\"form_container\">                                                                                                       "
						+ "	                                                                                                                                  "
						+ "		<h1><a>Logowanie - Formularz</a></h1>                                                                                                 "
						+ "		<form id=\"form_953125\" class=\"appnitro\"  method=\"post\" action=\"\">                                                     "
						+ "					<div class=\"form_description\">                                                                                  "
						+ "		</div>						                                                                                                  "
						+ "			<ul >                                                                                                                     "
						+ "			                                                                                                                          "
						+ "					<li id=\"li_1\" >                                                                                                 "
						+ "		<label class=\"description\" for=\"login\">Login                                                                          "
						+ " </label>                                                                                                                           "
						+ "		<div>                                                                                                                         "
						+ "			<input id=\"login\" name=\"login\" class=\"element text medium\" type=\"text\" maxlength=\"50\" value=\"\"/>     "
						+ "		</div>                                                                                                                        "
						+ "		</li>		<li id=\"li_2\" >                                                                                                 "
						+ "		<label class=\"description\" for=\"haslo\">Has�o </label>                                                                 "
						+ "		<div>                                                                                                                         "
						+ "			<input id=\"haslo\" name=\"haslo\" class=\"element text medium\" type=\"password\" maxlength=\"30\" value=\"\"/>     "
						+ "		</div>                                                                                                                        "
						+ "		</li>                                                                                                                         "
						+ "			                                                                                                                          "
						+ "					<li class=\"buttons\">                                                              "
						+ "			                                                                                                                          "
						+ "				<input id=\"saveForm\" class=\"button_text\" type=\"submit\" name=\"submit\" value=\"Submit\" />                      "
						+ "		</li>                                                                                                                         "
						+ "			</ul>                                                                                                                     "
						+ "		</form>	                                                                                                                      "
						+ "		<div id=\"footer\">                                                                                                           "
						+ "			                                                               "
						+ "		</div>                                                                                                                        "
						+ "	</div>                                                                                                                            ");

		resp.setCharacterEncoding("UTF-8");
		resp.setContentType("text/html");
		resp.getWriter().write(zawartosc);
	}
}